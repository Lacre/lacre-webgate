
<?php
/*

	gpg-mailgate

	This file is part of the gpg-mailgate source code.

	gpg-mailgate is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	gpg-mailgate source code is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with gpg-mailgate source code. If not, see <http://www.gnu.org/licenses/>.

 */
echo '<link rel="stylesheet" href="themes/'.$config['site_theme'].'/css/style.css" type="text/css">';
?>


 <div class="wrapper padding">
 <? if(!empty($message)) { ?>
 <div id=infomsg><p><?= htmlspecialchars($message) ?></p></div>
 <? } ?>
 <p>
 <input type="submit" value="<?= $lang['info_back'] ?>" onclick="parent.location='index.php'">
</p>
 </div>

