<?php
/*

	gpg-mailgate

	This file is part of the gpg-mailgate source code.

	gpg-mailgate is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	gpg-mailgate source code is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with gpg-mailgate source code. If not, see <http://www.gnu.org/licenses/>.

*/
?>

<html>
<head>
<title>Lacre - PGP key management</title>
</head>
<body>
<div id=header>
<div class="banner narrow">
<a href <?= $config['site_url'] ?>><img src="themes/<?= $config['site_theme'] ?>/<?= $config['site_logo'] ?>" class="center banner"></a>
</div>
  <div class="header">
    <nav class="main-nav">
    <ul>
      <li class="selected"><a href="/">Home</a></li>
      <li class=""><a href="<?= $config['site_faqurl'] ?>">FAQ</a></li>
      <li class=""><a href="<?= $config['site_howurl'] ?>">Help</a></li>
      <li class=""><a href="<?= $config['site_contacturl'] ?>">Contact</a></li>
    </ul>
    </nav>
  </div>
</div>
  <h1><?= $config['site_title'] ?></h1>

