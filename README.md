Lacre Webgate
--------------
This is Lacre's simple php based frontend. It has been forked from [gpg-mailgate](https://github.com/fkrone/gpg-mailgate) project and it is a continuation of the original work. Special thanks to all those who have contributed to amazing work of gpg-mailgate-web and those who are willing to contribute to Lacre to improve security of email storage.

For the backend, see [lacre](https://git.disroot.org/Disroot/gpg-lacre).

Lacre-Webgate is a simple web interface designed to allow any web user to upload their PGP public key and then have all mail sent via your mail server be encrypted. (Note: this is not meant for email authentication, only encryption.)

After submitting their key to a web form, the user will be required to confirm their email address. A cron script will register the public key with gpg-lacre (keyhome_only must be set to no currently, which is the default) after email confirmation. From then on, email to the specified address will be encrypted with the public key.

Lacre-Webgate frontend is useful for two purposes: 
 - for a transparent PGP encryption layer in front of any web application
 - as a web interface for gpg-lacre so that users on your mail server can easily upload and change their PGP keys.

Note that all processing relating to the mail server is done via the cron script. This means that gpg-Lacre and the web frontend cron can be installed on a different server from the web server. The MySQL database must be shared between the two applications though.

1. Installation instructions:
Below instructions assume working gpg-Lacre setup (backend) as well as working web-server (eg. nginx+php8) and sql database (eg. mariadb).

  1) Create a MySQL database for Lacre-Web.
    a) Schema file is located in schema.sql
    b) Database name and account goes in /etc/gpg-lacre.conf (and set enabled = yes)
  2) Copy the contents of public_html to your web directory.
  3) Move config.sample.php to config.php and edit the configuration file.
  4) Copy cron.py to /usr/local/bin/Lacre-web-cron.py and set up a cron job
    ```
    */3 * * * * lacre /usr/bin/python /usr/local/bin/Lacre-web-cron.py > /dev/null
    ```
  5) Enable PHP extensions: `mbstring`.
  6) Ensure that cron is working and test your new Lacre-web installation!

2. Customization: 
Lacre-Webgate allows for customization of css as well as templates. You can add your custom theme to `/themes/`. You can switch theme by adjusting `$config['site_theme']` in config.php` where the name is the theme's directory name. 
