-- lacre
--
-- This file is part of the lacre source code.
--
-- lacre is free software: you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation, either version 3 of the License, or (at your option) any later
-- version.
--
-- lacre source code is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
-- or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
-- more details.
--
-- You should have received a copy of the GNU General Public License along
-- with lacre source code. If not, see <http://www.gnu.org/licenses/>.

-- confirm is empty once an email address has been confirmed, and otherwise is
-- the confirmation key status
--    initializes to 0
--    is set to 1 after a public key with (confirm='', status=0) has been imported
--    is set to 2 if a key should be deleted (will be deleted based on email address)
-- publickey is the ASCII-armored PGP public key; can be cleared to save space if status > 0
CREATE TABLE lacre_keys (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	email VARCHAR(256),
	publickey TEXT,
	confirm VARCHAR(32),
	status INT NOT NULL DEFAULT 0,
	time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- see include/lock.php for documentation
CREATE TABLE lacre_locks (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	ip VARCHAR(16),
	time INT,
	action VARCHAR(16),
	num INT
);

-- GnuPG identities known to lacre.
--
-- This table can be used instead of the file-based key store (pubring.kbx).
-- Set lacre configuration option [keyring]type to 'database' to use this
-- table.
CREATE TABLE lacre_identities (
	email VARCHAR (256) NOT NULL PRIMARY KEY,
	fingerprint VARCHAR (64) NOT NULL
);
